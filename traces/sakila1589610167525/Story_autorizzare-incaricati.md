# Story autorizzare-incaricati

---
## Given

$$
\int \oint \sum \prod
$$

In physics, the mass-energy equivalence is stated 
by the equation $$E=mc^2$$ discovered in 1905 by Albert Einstein.



Example of equation inline


$$
\mathcal{RQSZ}
$$

$$
\mathfrak{RQSZ}
$$

$$
\mathbb{RQSZ}
$$

Un responsabile

## When

Quando un responsabile decide di autorizzare un incaricato

---
## Who

Il responsabile

## What

1. Accede al sistema
2. Accede al pannello incaricati
3. Aggiunge la persona come nuovo incaricato.
4. Comunica al nuovo incaricato le credenziali di primo accesso

---
## Then

La persona può accedere con le credenziali di primo accesso

## Examples